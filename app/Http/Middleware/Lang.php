<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //  Check header request and determine localizaton
        if ($request->hasHeader('Lang'))
            app()->setLocale($request->header('Lang'));

        return $next($request);
    }
}
