<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\AuthLoginRequest;
use App\Http\Requests\Auth\AuthRegisterRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function login(AuthLoginRequest $request): JsonResponse
    {
        $loginRequest = $request->validated();

        $remember = Arr::exists($loginRequest, 'remember') ? $loginRequest['remember'] : false;

        $loginCredentials = ['username' => $loginRequest['username'], 'password' => $loginRequest['password']];

        if (
            Auth::attempt($loginCredentials, $remember)
            ||
            Auth::attempt(['email' => $loginCredentials['username'], 'password' => $loginCredentials['password']], $remember)
        ) {
            /** @var \App\Models\User */
            $user = Auth::user();
            if ($user->isEmailVerified())
                return response()->json(['user' => $user, 'access_token' => $user->createToken('authToken')->accessToken], 200);

            throw ValidationException::withMessages(['all' => __("Account wasn't verified check your e-mail.")]);
        }
        throw ValidationException::withMessages(['username' => '', 'password' => '', 'all' => __("Invalid login data.")]);
    }

    public function register(AuthRegisterRequest $request): JsonResponse
    {
        $registerData = $request->validated();
        $user = User::create($registerData);

        return response()->json(['data' => $user, 'message' => __('Registered successfully.')], 200);
    }

    public function logout(Request $request): JsonResponse
    {
        $request->user()->token()->revoke();
        return response()->json(['message' => __('Logged out.')], 200);
    }
}
// Class App\Policies\ModelPolicy does not exist